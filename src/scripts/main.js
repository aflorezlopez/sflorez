import Vue from 'vue/dist/vue';
import VueRouter from 'vue-router';
import Header from './components/header';
import About from './components/about';
import Gsap from 'gsap';
import $ from 'jquery';

Vue.component('Navigation', Header);
Vue.component('About', About);

Vue.use(VueRouter);

const routes = [
	{path: "/about", component: About},
	{path: "/experience", component: Header},
	{path: "/works", component: Header},
	{path: "/contact", component: Header},
]

const router = new VueRouter({
	routes,
});

new Vue({
	router
}).$mount('#main')