import $ from "jquery";

export default {
	data() {
		return{
			title: "First",
			timeline: new TimelineMax({ paused: true, reversed: true }),
		};
	},

	template: "#header-template",

	mounted() {
		var listItems = document.querySelectorAll('ul#menu li');
		this.timeline.staggerFromTo(
			listItems, 
			1.2, 
			{ autoAlpha: 0, rotationX: -90, transformOrigin: '50% 0%' }, 
			{ autoAlpha: 1, rotationX: 0, ease: Elastic.easeOut.config(1, 0.3) }, 
			0.1, 
			0.3
		);
		TweenLite.set('h2, .scroll-prompt, .scroll-prompt2, .mouse', {visibility:"visible"});
	},

	methods: {
		showMenu(){
			var toggleMenu = $('.menu-toggle');
			toggleMenu.toggleClass('on');
			this.timeline.reversed() ? this.timeline.play() : this.timeline.reverse();
	  	this.timeline.staggerTo('h2, .scroll-prompt, .scroll-prompt2, .mouse', 0, {visibility:"hidden", immediateRender:false}, .25, .25);
		}
	}
}
